package edu.uah.services;

import edu.uah.model.ImageInfo;
import edu.uah.model.ScanResult;

public interface ImageScannerListener {
    void onImageScanned(ImageInfo info, ScanResult result);
}
