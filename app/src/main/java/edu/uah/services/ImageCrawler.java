package edu.uah.services;

public interface ImageCrawler {
    void crawlImages(String uri, String viewUri, String paginationParam, ImageCrawlerListener imageCrawlerListener);
}
