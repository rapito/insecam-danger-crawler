package edu.uah.services;

import edu.uah.model.ImageInfo;

public interface ImageScanner {
    void scanImage(ImageInfo info, ImageScannerListener listener);
}
