package edu.uah.services;

import edu.uah.model.CrawlResult;
import edu.uah.model.ImageInfo;

public interface ImageCrawlerListener {
    void onImageCrawled(ImageInfo info, CrawlResult result);
}
