package edu.uah.services.impl;

import edu.uah.model.CrawlResult;
import edu.uah.model.ImageInfo;
import edu.uah.services.ImageCrawler;
import edu.uah.services.ImageCrawlerListener;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class InseCamCrawler implements ImageCrawler {

    private static ImageCrawler instance;
    private int maxPages = 1;

    public static ImageCrawler create() {
        return instance == null ? instance = new InseCamCrawler() : instance;
    }

    @Override
    public void crawlImages(String uri, String viewUri, String paginationParam, ImageCrawlerListener imageCrawlerListener) {

        for (int i = 1; i <= maxPages; i++) {
            String targetURL = getURL(uri, paginationParam, i);

            new Thread() {
                @Override
                public void run() {
                    super.run();
                    crawlPage(viewUri, targetURL, imageCrawlerListener);
                }
            }.start();
        }
    }

    private void crawlPage(String viewUrl, String targetURL, ImageCrawlerListener imageCrawlerListener) {
        try {

            Document doc = Jsoup.connect(targetURL).get();
            Elements elements = doc.select(".thumbnail-item__img");

            for (Element cam : elements) {

                String id = cam.attr("id").replace("image", "");

                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        crawlView(String.format("%s/%s", viewUrl, id), imageCrawlerListener);
                    }
                }.start();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void crawlView(String targetUrl, ImageCrawlerListener imageCrawlerListener) {
        try {
            ImageInfo info = new ImageInfo();

            Document doc = Jsoup.connect(targetUrl).get();
            Elements elements = doc.select(".camera-details__cell");
            String linkSelector = "camera-details__link";

            info.imageURL = doc.select("#image0").attr("src");
            info.addToMap("url", targetUrl);

            for (int i = 0; i < elements.size(); i += 2) {
                try {
                    if (i >= elements.size()) continue;

                    String key = elements.get(i).html().replace(":", "");
                    String value = elements.get(i + 1).html();

                    if (value.contains(linkSelector)) {
                        value = elements.get(i + 1).selectFirst(String.format(".%s", linkSelector)).html();
                    }

                    info.addToMap(key, value);
                } catch (Exception e) {
                    // e.printStackTrace();
                }

            }

            imageCrawlerListener.onImageCrawled(info, new CrawlResult(targetUrl, true));

        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    private String getURL(String url, String paginationParam, int page) {
        return String.format("%s?%s=%d", url, paginationParam, page);
    }

}
