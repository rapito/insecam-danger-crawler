package edu.uah.services.impl;

import com.google.cloud.vision.v1.*;
import com.google.protobuf.ByteString;
import edu.uah.model.ImageInfo;
import edu.uah.model.ScanResult;
import edu.uah.services.ImageScanner;
import edu.uah.services.ImageScannerListener;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GoogleCloudScanner implements ImageScanner {

    static GoogleCloudScanner instance;

    public static ImageScanner create() {
        return instance == null ? instance = new GoogleCloudScanner() : instance;
    }

    @Override
    public void scanImage(ImageInfo info, ImageScannerListener listener) {

        try {
            // Download and Send Image to Google
            Likelihood violence = getLikelihood(info);

            if (violence == null) return;

            boolean isDangerous = violence.getNumber() >= Likelihood.POSSIBLE_VALUE;

            listener.onImageScanned(info, new ScanResult(isDangerous, true));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Likelihood getLikelihood(ImageInfo info) throws IOException {
        ImageAnnotatorClient vision = ImageAnnotatorClient.create();

        List<AnnotateImageRequest> requests = new ArrayList<>();

        Image img = getImage(info);

        Feature feat = Feature.newBuilder().setType(Feature.Type.SAFE_SEARCH_DETECTION).build();
        AnnotateImageRequest request = AnnotateImageRequest.newBuilder()
                .addFeatures(feat)
                .setImage(img)
                .build();
        requests.add(request);

        BatchAnnotateImagesResponse response = vision.batchAnnotateImages(requests);
        List<AnnotateImageResponse> responses = response.getResponsesList();

        Likelihood violence = Likelihood.UNKNOWN;

        for (AnnotateImageResponse res : responses) {
            if (res.hasError()) {
                return null;
            }

            SafeSearchAnnotation annotation = res.getSafeSearchAnnotation();
            violence = annotation.getViolence().getNumber() > violence.getNumber() ? annotation.getViolence() : violence;
        }
        return violence;
    }

    private Image getImage(ImageInfo info) throws IOException {

        boolean download = true;

//        info.imageURL = "https://www.thelocal.es/userdata/images/article/781d5563d0708fc4eb83db48a785206577aed53b31add9e4dbb2b566fdf24915.jpg";

        if (!download) {
            return getImageFromGoogle(info);
        } else {
            return getImageDownloaded(info);
        }

    }

    private Image getImageDownloaded(ImageInfo info) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(ImageIO.read(new URL(info.imageURL)), "jpg", baos);
        byte[] bytes = baos.toByteArray();

        return Image.newBuilder()
                .setContent(ByteString.copyFrom(bytes)).build();
    }

    private Image getImageFromGoogle(ImageInfo info) {
        ImageSource imageSource = ImageSource.newBuilder().setImageUri(info.imageURL).build();

        return Image.newBuilder()
                .setSource(imageSource)
                .build();
    }
}
