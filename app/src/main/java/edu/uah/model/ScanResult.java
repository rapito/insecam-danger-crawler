package edu.uah.model;

public class ScanResult {

    private boolean successful;
    private boolean dangerous;

    public ScanResult(boolean isDangerous, boolean successful) {
        this.dangerous = isDangerous;
        this.successful = successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public boolean isDangerous() {
        return dangerous;
    }

    public void setDangerous(boolean dangerous) {
        this.dangerous = dangerous;
    }
}
