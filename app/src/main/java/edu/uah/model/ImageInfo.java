package edu.uah.model;

import java.util.HashMap;
import java.util.Map;

public class ImageInfo {
    private HashMap<String, String> map = new HashMap<>();
    public String imageURL;

    private String url() {
        if (this.map.containsKey("url")) return this.map.get("url");
        return null;
    }

    public String info() {

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("image: %s ", imageURL));
        for (Map.Entry e : map.entrySet()) {
            sb.append(String.format("%s: %s ", e.getKey(), e.getValue()));
        }
        return sb.toString();
    }

    public void addToMap(String key, String value) {
        this.map.put(key, value);
    }

    @Override
    public String toString() {
        return String.format("url: %s", url());
    }
}
