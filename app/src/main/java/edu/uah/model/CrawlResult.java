package edu.uah.model;

public class CrawlResult {
    private boolean successful;
    private String url;

    public CrawlResult(String url, boolean successful) {
        this.url = url;
        this.successful = successful;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
