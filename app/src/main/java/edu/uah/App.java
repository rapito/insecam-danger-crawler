package edu.uah;

import edu.uah.model.CrawlResult;
import edu.uah.model.ImageInfo;
import edu.uah.model.ScanResult;
import edu.uah.services.*;
import edu.uah.services.impl.GoogleCloudScanner;
import edu.uah.services.impl.InseCamCrawler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App implements ImageScannerListener, ImageCrawlerListener {

    static String URI = "http://www.insecam.org/en/byrating";
//    static String URI = "http://www.insecam.org/en/bycountry/ES";
    static String VIEW_URI = "http://www.insecam.org/en/view";
    static String PAGINATION_PARAM = "page";

    public static void main(String[] args) {
        new App().start();
    }

    List<ImageInfo> images = Collections.synchronizedList(new ArrayList<ImageInfo>());
    List<ImageInfo> dangerImages = Collections.synchronizedList(new ArrayList<ImageInfo>());

    private ImageScanner scanner;
    private ImageCrawler crawler;

    private void start() {
        this.scanner = GoogleCloudScanner.create();
        this.crawler = InseCamCrawler.create();

        crawler.crawlImages(URI, VIEW_URI, PAGINATION_PARAM, (ImageCrawlerListener) this);
    }

    public void onImageCrawled(ImageInfo image, CrawlResult result) {
        images.add(image);

        new Thread(){
            @Override
            public void run() {
                super.run();
                scanner.scanImage(image, App.this);
            }
        }.start();
    }

    public void onImageScanned(ImageInfo image, ScanResult result) {
        if (result.isDangerous()) {
            dangerImages.add(image);
            System.err.println(String.format("Something is happening here: %s", image.info()));
        } else {
            System.out.println(String.format("%s is fine", image));
        }
    }

}
